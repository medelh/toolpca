<?php
require 'vendor/autoload.php';

use Kachkaev\PHPR\RCore;
use Kachkaev\PHPR\Engine\CommandLineREngine;

$r = new RCore(new CommandLineREngine('/usr/bin/R'));

$result = $r->run(<<<EOF
x = 1
y = 2
x + y
x + z
x - y
EOF
    );

echo $result;

if (isset($_POST['submit'])) {

    $N = 10;
 
    // execute R script from shell
    // this will save a plot at temp.png to the filesystem
    print_r(exec("whoami"));
    exec("./script.sh");
    print_r(exec("whoami"));
    //shell_exec("python script.py");
    // return image tag
    /*
    $dir = "/opt/Ngs_analysis/Sharedwww/Genomica_TOT/NGS/ToolPCAadenovirus/";
    $fastatxt = $_POST['fastatxt'];
    if (strlen($fastatxt) > 0) {
        $myfile = fopen($dir."temp.fasta", "w") or die("Unable to open file!");
        fwrite($myfile, $fastatxt);
        fclose($myfile);
    } else {
        $file = $_FILES['file'];

        $fileName = $_FILES['file']['name'];
        $fileTmpName = $_FILES['file']['tmp_name'];
        $fileSize = $_FILES['file']['size'];
        $fileError = $_FILES['file']['error'];
        $fileType = $_FILES['file']['type'];

        $fileExt = explode('.', $fileName);
        $fileActualExt = strtolower(end($fileExt));
        $allowed = array('fa', 'mpfa', 'fna', 'fsa', 'fas', 'fasta', 'txt');
        if (in_array($fileActualExt, $allowed)) {
            if ($fileError === 0) {
                if ($fileSize < 10000000) {
                    $fileDestination = $dir.'temp.fasta';
                    move_uploaded_file($fileTmpName, $fileDestination);
                } else {
                    echo "Error uploading... Your file is too big";
                }
            } else {
                echo "Error uploading... There was an error uploading your file";
            }
        } else {
            echo "Error uploading... You cannot upload files of this type";
        }
    }
    // todo
    // call the program (not found)
    // $result = exec("python /opt/Ngs_analysis/Sharedwww/Genomica_TOT/NGS/ToolPCAadenovirus/helloworld.py");
    // echo($result);
    $cmd = "/usr/bin/Rscript /opt/Ngs_analysis/Sharedwww/Genomica_TOT/NGS/ToolPCAadenovirus/scriptCodonUsage.R /opt/Ngs_analysis/Sharedwww/Genomica_TOT/NGS/ToolPCAadenovirus/temp.fasta &";
    exec($cmd, $output);
    echo($output);*/
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- js -->
    <!-- <script src="./index.js"></script> -->
    <!-- css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Tool PCA adenovirus</title>
</head>

<body>
    <div class="container well">
        <h1>Tool PCA adenovirus</h1>
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
            <div class="form-group" id="divFile">
                <label for="exampleFormControlFile1">Update fasta file</label>
                <input type="file" class="form-control-file" id="inputFile" name="file" accept=".fa,.mpfa,.fna,.fsa,.fas,.fasta,.txt">
                <p class="text-danger" id="errorTypeFile">ERROR! extension not allowed</p>
            </div>

            <div class="form-group" id="divText">
                <label for="lblTextareaFasta">Paste the raw sequence or one or more FASTA sequences into the text area. Input limit is 10,000,000 characters.</label>
                <textarea class="form-control" id="txtareaFasta" name="fastatxt" rows="10" maxlength="10000000" style="resize: none; border-width: 2px;"></textarea>
                <p class="text-danger" id="errorTextFasta">ERROR! this is not a correct fasta text</p>
            </div>
            <input type="submit" value="Submit" id="submit" name="submit">
            <input type="button" value="Clear" id="clear">
        </form>
    </div>

    <script>
        $(document).ready(function() {
            onload();

            //on click clear button call the onlad function
            $("#clear").click(function() {
                onload();
            });

            //checks if the file of the input file is valid ands shows/hide errors and enable/disable buttons
            $("#inputFile").change(function() {
                if ($("#inputFile")[0].files.length == 1) {
                    // Compare extensión of the file with the valid extensions
                    validextension = ["fa", "mpfa", "fna", "fsa", "fas", "fasta", "txt"];
                    if (validextension.indexOf($("#inputFile")[0].value.split(".").slice(-1)[0]) > -1) {
                        $("#submit").prop('disabled', false);
                        $("#errorTypeFile").hide();
                    } else {
                        $("#errorTypeFile").show();
                    }
                    $("#clear").prop('disabled', false);
                    $("#divText").children().prop('disabled', true);
                }
            });

            //checks if the textarea is valid and shows/hide errors and enable/Disable buttons
            $('#txtareaFasta').bind('input propertychange', function() {
                if ($('#txtareaFasta')[0].value.length > 0) {
                    if (isValidFasta()) {
                        $("#txtareaFasta").css("border-color", "green");
                        $("#submit").prop('disabled', false);
                        $("#errorTextFasta").hide();
                    } else {
                        $("#txtareaFasta").css("border-color", "red");
                        $("#submit").prop('disabled', true);
                        $("#errorTextFasta").show();
                    }
                    $("#divFile").children().prop('disabled', true);
                    $("#clear").prop('disabled', false);
                } else {
                    onload();
                }
            });
        });

        /**
         * @name onload()
         * @description function that resets the form, hide the error messages, and enable/dissable some components, also sets the border color of the textarea
         */
        function onload() {
            $("form")[0].reset();
            $("#submit").prop('disabled', true);
            $("#clear").prop('disabled', true);
            $("#errorTypeFile").hide();
            $("#errorTextFasta").hide();
            $("#divFile").children().prop('disabled', false);
            $("#divText").children().prop('disabled', false);
            $("#txtareaFasta").css("border-color", "RGB(206,212,218)");
        }

        /**
         * @name isValidFasta
         * @description function that checks if the text is a valid format fasta or not, returns true if is valid, false otherwise
         * @returns boolean
         */
        function isValidFasta() {
            var fasta = $("#txtareaFasta").val();
            var regexFasta = /^((>.*\n)?([atcgATCG]+\n*)+)*$/gm;
            if (fasta.match(regexFasta) != null) {
                return (fasta.length == fasta.match(regexFasta)[0].length)
            }
            return false;
        }
    </script>
</body>

</html>
